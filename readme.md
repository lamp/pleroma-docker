# Pleroma Docker

Pleroma from-source installation in Docker with RUM indexing. Pleroma source code is mounted from host directory so you can easily modify it without rebuilding images.

Tip: Always install latest version of Docker! https://docs.docker.com/engine/install/

1. Download repository and enter directory.
2. Edit docker-compose.yml, set a secure db admin password, change ports if necessary
3. `docker compose build`
4. `git clone https://git.pleroma.social/pleroma/pleroma.git -b stable`
5. `mkdir -p data/{static,uploads}; sudo chown -R 888:888 pleroma data/{static,uploads}`
6. `docker compose run --rm --no-deps pleroma mix deps.get`
7. `docker compose run --rm --no-deps pleroma mix pleroma.instance gen`
	1. Use defaults for db
	2. `y` at RUM indices
	3. Default `4000` for port
	4. Listen on `0.0.0.0`!
	5. Media uploads in `/uploads`
	6. "Custom public files" in `/static`
8. `sudo mv pleroma/config/{generated_config.exs,prod.secret.exs}`
9. `sudo sed -i 's~hostname: "localhost"~socket_dir: "/run/postgresql/"~' pleroma/config/prod.secret.exs`
10. `docker compose up -d postgres`
	- Check if setup_db.sql ran successfully: `docker compose logs postgres`
11. `docker compose run --rm pleroma mix ecto.migrate`
12. `docker compose run --rm pleroma mix ecto.migrate --migrations-path priv/repo/optional_migrations/rum_indexing/`
13. If you wanted to store config in db: `docker compose run --rm pleroma mix pleroma.config migrate_to_db`
14. `docker compose up -d pleroma`
	- Check if running: `curl localhost:4000`

Now connect your reverse proxy. For Caddy (recommended):

```caddyfile
pleroma.example {
	reverse_proxy 127.0.0.1:4000
}
```

Plus separate media domain (recommended):

```caddyfile
cdn.pleroma.example {
	root * /srv/pleroma-docker/data/uploads
	uri strip_prefix /media/ # if you kept this in the base url
	file_server
}
```

Make sure caddy can read pleroma upload directory.

Once the website is working, create admin user: `docker compose exec pleroma mix pleroma.user new admin admin@yourserver --admin`


### Optional

Create pleroma user on host system: `sudo useradd -r -u 888 pleroma`

Easier access to config: `ln -s pleroma/config/prod.secret.exs`
